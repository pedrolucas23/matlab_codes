function [saida, t] = bpsk(entrada, freq_portadora)

    t = linspace(0,1,length(entrada));
    saida = linspace(0,1,length(entrada));

    for i=1:length(entrada)
    
        if(entrada(i) == 0)
            saida(i) = cos(2*pi*freq_portadora*t + pi);
        else
            saida(i) = cos(2*pi*frequ_portadora*t + 2*pi);                        
        end
        
    end

end