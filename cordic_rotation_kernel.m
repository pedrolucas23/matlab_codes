function [x, y, z] = cordic_rotation_kernel(x, y, z)
% Perform CORDIC rotation kernel algorithm for N iterations.
n = 30;
xtmp = x;
ytmp = y;
inpLUT = atan(2 .^ -(0:n-1));
for idx = 1:n
    if z < 0
        z(:) = accumpos(z, inpLUT(idx));
        x(:) = accumpos(x, ytmp);
        y(:) = accumneg(y, xtmp);
    else
        z(:) = accumneg(z, inpLUT(idx));
        x(:) = accumneg(x, ytmp);
        y(:) = accumpos(y, xtmp);
    end
    xtmp = bitsra(x, idx); % bit-shift-right for multiply by 2^(-idx)
    ytmp = bitsra(y, idx); % bit-shift-right for multiply by 2^(-idx)
end