%CORIC Gr�ficos

%function [saida_x, saida_y, an] = grafCordic(e_x, e_y, e_teta, itera)
function [saida_x, saida_y] = grafCordic(e_x, e_y, e_teta, itera)

    %sigma = 1;
    LUT = atan(2 .^ -(0:itera));
    aux_xi = zeros(1,itera+1);
    aux_yi = zeros(1,itera+1);
    aux_zi = zeros(1,itera+1);
    ki = zeros(1,itera+1);

    for i = 0 : itera
    
        if(i == 0)%Primeira itera��o
            
            aux_xi(1) = e_x - bitsra(e_y, i);%(e_y*(2^(-i)));
            aux_yi(1) = e_y + bitsra(e_x, i);%(e_x*(2^(-i)));
            aux_zi(1) = e_teta - (LUT(i+1));
            
            ki(1) = sqrt(1+2^(-2*(i)));
            
        else%demais itera��es
            
            if(aux_zi(i) < 0)%dire��o da rota��o
                %sigma = -1;
                aux_xi(i+1) = aux_xi(i) + bitsra(aux_yi(i),i);%(aux_yi(i)*(2^(-i)));
                aux_yi(i+1) = aux_yi(i) - bitsra(aux_xi(i),i);%(aux_xi(i)*(2^(-i)));
                aux_zi(i+1) = aux_zi(i) + (LUT(i+1));
            else
                %sigma = 1;
                aux_xi(i+1) = aux_xi(i) - bitsra(aux_yi(i),i);%(aux_yi(i)*(2^(-i)));
                aux_yi(i+1) = aux_yi(i) + bitsra(aux_xi(i),i);%(aux_xi(i)*(2^(-i)));
                aux_zi(i+1) = aux_zi(i) - (LUT(i+1));
            end
            
            %C�lculo do Kn
            ki(i+1) = ki(i)*sqrt(1+2^(-2*(i)));            
            
        end      
        
    end
    
%     eixo_x = linspace(0,itera+1,itera+1);
%     
%     subplot(2,1,1);
%     plot(eixo_x,aux_xi,'-k');
%     grid on;
%     xlabel('Itera��es');
%     ylabel('Valor de cosseno');
%     
%     subplot(2,1,2);
%     plot(eixo_x,aux_yi,'-r');
%     grid on;
%     xlabel('Itera��es');
%     ylabel('Valor de seno');
    
    saida_x = aux_xi(itera+1)/ki(itera+1);
    saida_y = aux_yi(itera+1)/ki(itera+1);
    %an = ki(itera+1);
    
end