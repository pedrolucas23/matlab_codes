% Implementa��o do CORDIC

%
%   No c�digo havia dois erros:
%       - O primeiro: A primeira itera��o come�a em 0, n�o em 1 como os
%       vetores do MATLAB;
%
%       - O segundo: O valor de Kn � o ac�mulo do erro residual de cada
%       itera��o, e pra retirar esse erro, tem que dividir o valor de Kn
%       pelos valores de Xn e Yn ao final para se obter o valor correto ao
%       final.
%

function [s_x, s_y, k, s_z] = impCordic(e_x, e_y, e_teta, itera)

    sigma = 1;

    for i = 0 : itera
    
        if(i == 0)%Primeira itera��o
            
            aux_xi = e_x - (e_y*sigma*(2^(-i)));
            aux_yi = e_y + (e_x*sigma*(2^(-i)));
            aux_zi = e_teta - (sigma*atan(2^(-i)));
            
            ki = sqrt(1+2^(-2*(i)));
            
        else%demais itera��es
            
            if(aux_zi < 0)%dire��o da rota��o
                sigma = -1;
            else
                sigma = 1;
            end
            
            %Itera��es sucessivas de acordo com as f�rmulas do artigo
            aux_xii = aux_xi - (aux_yi*sigma*(2^(-i)));
            aux_yii = aux_yi + (aux_xi*sigma*(2^(-i)));
            aux_zii = aux_zi - (sigma*atan(2^(-i)));
            
            %C�lculo do Kn
            kii = ki*sqrt(1+2^(-2*(i)));            
            
            aux_xi = aux_xii;
            aux_yi = aux_yii;
            aux_zi = aux_zii;
            
            ki = kii;
            
        end      
        
    end

    s_x = aux_xi/ki;
    s_y = aux_yi/ki;
    k = ki;
    s_z = aux_zi;

end