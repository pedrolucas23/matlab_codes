function [saida, t] = modula_bpsk(entrada, freq_portadora)

    %sign = [zeros(1,100), ones(1,100), zeros(1,100), ones(1,200), zeros(1,100), ones(1,400)];

    t = linspace(0,1,length(entrada));
    saida = linspace(0,1,length(entrada));
    sinal_0 = sin(2*pi*freq_portadora*t+pi);
    sinal_1 = sin(2*pi*freq_portadora*t+(pi*2));

    for i=1:length(entrada)
    
        if(entrada(i) == 0)
            saida(i) = sinal_0(i);
        else
            saida(i) = sinal_1(i);                      
        end
        
    end
    
    subplot(2,1,1);
    plot(t,saida);
    grid on;
    
    subplot(2,1,2);
    stem(t,entrada);
    grid on;

end