function x=mychirp(t,f0,f1)

    x = linspace(t(1),t(length(t)), length(t));
    
    t0=t(1);
    t1 = t(length(t));
    T=t1-t0;
    k=(f1-f0)/T;
    
    for i=1:length(t)        
        x(i) = sign(sin(2*pi*(k/2*t(i)+f0)*t(i)));  
        if x(i) < 0
            x(i) = 0;
        end
    end
end