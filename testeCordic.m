function testeCordic(inter1, inter2)

    t = linspace(inter1, inter2, 10000);
    y = linspace(inter1, inter2, 10000);
    x = linspace(inter1, inter2, 10000);
    %fc = 1;
    %constSeno = 2*pi*fc*t;
    constSeno = t;
    
    for i=1:length(t)
        %[x(i), y(i)] = grafCordic(1,0,2*pi*fc*t(i),25);
        %[x(i), y(i)] = cordic_rotation_kernel(0.6073,0,t(i));
        %[x(i), y(i)] = grafCordic(1,0,t(i),25);
        [x(i), y(i)] = cordic(0.6073,0,t(i),25,0);
    end
    
    figure;
    subplot(3,2,1);    
    plot(t,sin(constSeno));
    title('Seno MATLAB');
%     axis([-0.1 1.6 -0.1 1.1]);
    grid on;
    subplot(3,2,3);
    plot(t,y);
    title('Seno CORDIC');
%     axis([-0.1 1.6 -0.1 1.1]);
    grid on;
    
    errs = sin(constSeno) - y;
    
    subplot(3,2,5);
    plot(t, errs);
    title('Erro entre Seno MATLAB e Seno CORDIC');    
    grid on;
    
    subplot(3,2,2);    
    plot(t,cos(constSeno));
    title('Cosseno MATLAB');
%     axis([-0.1 1.6 -0.1 1.1]);
    grid on;
    subplot(3,2,4);
    plot(t,x);
    title('Cosseno CORDIC');
%     axis([-0.1 1.6 -0.1 1.1]);
    grid on;
    
    errc = cos(constSeno) - x;
    
    subplot(3,2,6);
    plot(t, errc);
    title('Erro entre Cosseno MATLAB e Cosseno CORDIC');
    grid on;

end